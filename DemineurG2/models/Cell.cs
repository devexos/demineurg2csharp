﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemineurG2.models
{
    public class Cell
    {
        protected bool _HasBomb = false;
        private int _NbBombsAround = 0;

        public bool HasBomb
        {
            get { return _HasBomb; }
        }

        public int NbBombsAround
        {
            get
            {
                return _NbBombsAround;
            }
        }

        public bool IsVisible { get; internal set; }

        public void PlaceBomb()
        {
            _HasBomb = true;
        }

        public void incBombsAround()
        {
            _NbBombsAround++;
        }

        public void Show()
        {
            IsVisible = true;
        }
    }
}
