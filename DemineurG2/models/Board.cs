﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemineurG2.models
{
    public class Board
    {
        protected Cell[,] _Cells;
        int _NbBombs = 30;
        int _Lines;
        int _Columns;



        public Board(int columns, int lines)
        {
            this._Lines = lines;
            this._Columns = columns;
            this._Cells = new Cell[columns, lines];
            CreateCells(columns, lines);
            PlaceBombs();
            // afficher les bombes dans le main
        }

        public bool isComplete
        {
            get
            {
                for (int x = 0; x < Columns; x++)
                {
                    for (int y = 0; y < Lines; y++)
                    {
                        if (!_Cells[x, y].IsVisible && !_Cells[x, y].HasBomb)
                        {
                            return false;
                        }
                    }

                }
                return true;
            }
        }
        public bool isExploded
        {
            get
            {
                for (int x = 0; x < Columns; x++)
                {
                    for (int y = 0; y < Lines; y++)
                    {
                        if (_Cells[x, y].IsVisible && _Cells[x, y].HasBomb)
                        {
                            return true;
                        }
                    }

                }
                return false;
            }
        }

        private void CreateCells(int columns, int lines)
        {
            for (int x = 0; x < columns; x++)
            {
                for (int y = 0; y < lines; y++)
                {
                    this._Cells[x, y] = new Cell();
                }
            }
        }

        public void ShowCellAt(int x, int y)
        {
            if (PositionIsInBoard(x, y))
            {
                if (_Cells[x, y].IsVisible)
                {
                    return;
                }
                this.ShowAround(x, y);
                _Cells[x, y].Show();
            }
        }

        private void ShowAround(int x, int y)
        {
            if (!this.PositionIsInBoard(x, y))
            {
                return;
            }
            if (getCellAt(x, y).HasBomb)
            {
                return;
            }
            if (getCellAt(x, y).IsVisible)
            {
                return;
            }
            _Cells[x, y].Show();
            if (getCellAt(x, y).NbBombsAround > 0)
            {
                return;
            }
            ShowAround(x + 1, y);
            ShowAround(x - 1, y);
            ShowAround(x, y + 1);
            ShowAround(x, y - 1);
        }
        private void PlaceBombs()
        {
            Random random = new Random();
            for(int i = 0; i < _NbBombs; i++) { 
                PlaceBombAt(random.Next(Columns-1), random.Next(Lines - 1));
            }
        }

        private void PlaceBombAt(int x, int y)
        {
            if (this._Cells[x, y].HasBomb)
                return;
            this._Cells[x, y].PlaceBomb();
            CountBombsAround(x, y);
        }

        private void CountBombsAround(int x, int y)
        {
            for (int dx = -1; dx <= 1; dx++)
            {
                for (int dy = -1; dy <= 1; dy++)
                {
                    if (dx == 0 && dy == 0)
                    {
                        continue;
                    }
                    if (this.PositionIsInBoard(x + dx, y + dy))
                    {
                        _Cells[x + dx, y + dy].incBombsAround();
                    }
                }

            }
        }

        private bool PositionIsInBoard(int x, int y)
        {
            return x >= 0 && y >= 0 && x < Columns && y < Lines;
        }

        public Cell getCellAt(int x, int y)
        {
            return _Cells[x, y];
        }

        public int Lines
        {
            get
            {
                return _Lines;
            }
        }

        public int Columns
        {
            get
            {
                return _Columns;
            }

        }


    }
}