﻿using DemineurG2.models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DemineurG2
{
    public partial class Form1 : Form
    {
        private const int CELL_SIZE = 25;
        protected Board board;
        public Form1(Board board)
        {
            this.board = board;
            InitializeComponent();
            InitializeButtons();
        }

        private void InitializeButtons()
        {
            this.SuspendLayout();
            this.buttonsPanel.Controls.Clear();
            for (int x = 0; x < board.Columns; x++)
                for (int y = 0; y < board.Lines; y++)
                {
                    Cell cell = board.getCellAt(x, y);
                    if (cell.IsVisible)
                    {
                        CreateLabel(x, y, cell);
                    }
                    else
                    {
                        CreateButton(x, y);
                    }
                }
            this.ResumeLayout(false);

        }

        private void CreateButton(int x, int y)
        {
            Button button = new Button();
            button.Location = new System.Drawing.Point(x * CELL_SIZE, y * CELL_SIZE);
            button.Size = new System.Drawing.Size(CELL_SIZE, CELL_SIZE);
            button.TabIndex = 0;
            button.Text = "";
            button.UseVisualStyleBackColor = true;
            button.Tag = new List<int>() { x, y };
            button.Click += new System.EventHandler(this.button_Click);
            this.buttonsPanel.Controls.Add(button);
        }

        private void CreateLabel(int x, int y, Cell cell)
        {
            Label label = new Label();
            label.BackColor = System.Drawing.SystemColors.ActiveCaption;
            //                        label.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            label.Location = new System.Drawing.Point(x * CELL_SIZE, y * CELL_SIZE);
            label.Size = new System.Drawing.Size(CELL_SIZE, CELL_SIZE);
            label.TabIndex = 0;
            label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            if (cell.HasBomb)
            {
                label.Text = "B";
            }
            else if (cell.NbBombsAround > 0)
            {
                label.Text = cell.NbBombsAround.ToString();
            }
            else
            {
                label.Text = "";
            }
            this.buttonsPanel.Controls.Add(label);
        }

        private void button_Click(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            List<int> position = (List<int>) button.Tag;
            board.ShowCellAt(position[0], position[1]);
            InitializeButtons();
            if (board.isExploded)
            {
                statusLabel.Text = "Perdu !";
            } else if (board.isComplete)
            {
                statusLabel.Text = "Gagné !";
            }
        }

    }
}
