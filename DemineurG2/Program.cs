﻿using DemineurG2.models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DemineurG2
{
    static class Program
    {
        /// <summary>
        /// Point d'entrée principal de l'application.
        /// </summary>
        [STAThread]
        static void Main()
        {

            Board board = new Board(30, 20);
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1(board));
        }

        private static void PrintBoard(Board board)
        {
            Console.WriteLine("Board: ");
            Console.WriteLine("******");
            for (int y = 0; y < board.Lines; y++)
            {
                for (int x = 0; x < board.Columns; x++)
                {
                    Cell cell = board.getCellAt(x, y);
                    if (!cell.IsVisible) {
                        Console.Write('_');
                    }
                    else if (cell.HasBomb)
                    {
                        Console.Write('B');
                    }
                    else if (cell.NbBombsAround > 0)
                    {
                        Console.Write(cell.NbBombsAround);
                    }
                    else
                    {
                        Console.Write(' ');
                    }
                    Console.Write(' ');
                }
                Console.WriteLine();
            }
        }
    }
}
